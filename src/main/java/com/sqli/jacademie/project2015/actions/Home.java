package com.sqli.jacademie.project2015.actions;

import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.interceptor.SessionAware;

import java.util.Map;

/**
 * Created by edenayrolles on 18/12/2015.
 */
public class Home extends ActionSupport implements SessionAware {

    private Map<String, Object> session = null;


    public String execute() {
        return SUCCESS;
    }

    @Override
    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
}
